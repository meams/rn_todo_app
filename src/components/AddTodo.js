import React, { useState } from 'react';
import {
  View, StyleSheet, TextInput, Alert, Keyboard,
} from 'react-native';
import { AntDesign } from '@expo/vector-icons';

// consts
import { THEME } from '../theme';

export const AddTodo = ({ onSubmit }) => {
  const { block, input } = styles;

  // hooks
  const [value, setValue] = useState('');

  const pressHandler = () => {
    if (value.trim()) {
      onSubmit(value);
      setValue('');
      Keyboard.dismiss();
    } else {
      Alert.alert('Empty data');
    }
  };

  return (
    <View style={block}>
      <TextInput
        style={input}
        onChangeText={setValue}
        value={value}
        placeholder="Enter todo..."
        autoCorrect={false}
        autoCapitalize="none"
      />
      <AntDesign.Button name="pluscircleo" onPress={pressHandler}>
        Add
      </AntDesign.Button>
      {/* <Button title="Add" onPress={pressHandler} /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  block: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15,
  },
  input: {
    width: '70%',
    borderStyle: 'solid',
    borderBottomWidth: 2,
    borderBottomColor: THEME.MAIN_COLOR,
    padding: 10,
  },
});
