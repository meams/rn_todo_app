import React from 'react';
import { View, StyleSheet, Platform } from 'react-native';

// consts
import { THEME } from '../theme';
import { AppTextBold } from './ui/AppTextBold';

export const Navbar = ({ title }) => {
  const { navbar, text } = styles;

  return (
    <View
      style={{
        ...navbar,
        ...Platform.select({
          ios: styles.navbarIos,
          android: styles.navbarAndroid,
        }),
      }}
    >
      <AppTextBold
        style={{
          ...text,
          ...Platform.select({
            ios: styles.textIos,
            android: styles.textAndroid,
          }),
        }}
      >
        {title}
      </AppTextBold>
    </View>
  );
};

const styles = StyleSheet.create({
  navbar: {
    height: 70,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: 10,
  },
  navbarAndroid: {
    backgroundColor: THEME.MAIN_COLOR,
  },
  navbarIos: {
    borderBottomColor: THEME.MAIN_COLOR,
    borderBottomWidth: 1,
  },
  text: {
    fontSize: 20,
  },
  textAndroid: { color: '#fff' },
  textIos: {
    color: THEME.MAIN_COLOR,
  },
});
