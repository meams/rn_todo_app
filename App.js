import React, { useState } from 'react';
import { StyleSheet, View, Alert } from 'react-native';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';

// components
import { Navbar } from './src/components/Navbar';
import { MainLayout } from './src/MainLayout';
import { TodoState } from './src/context/todo/TodoSate';
import { ScreenState } from './src/context/screen/ScreenState';

const loadApplication = async () => {
  await Font.loadAsync({
    'roboto-regular': require('./assets/fonts/Roboto-Regular.ttf'),
    'roboto-bold': require('./assets/fonts/Roboto-Bold.ttf'),
  });
};

export default function App() {
  // hooks
  const [isReady, setIsReady] = useState(false);

  if (!isReady) {
    return (
      <AppLoading
        startAsync={loadApplication}
        onError={(err) => console.log('AppLoad error', err)}
        onFinish={() => setIsReady(true)}
      />
    );
  }

  return (
    <ScreenState>
      <TodoState>
        <MainLayout />
      </TodoState>
    </ScreenState>
  );
}
